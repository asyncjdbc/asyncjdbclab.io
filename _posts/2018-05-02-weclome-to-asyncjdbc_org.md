---
layout: post
title:  "Welcome to asyncjdbc.org"
date:   2018-05-02 15:35:00 +0200
categories: news
---
asyncjdbc is a community fork of the [ADBA-over-JDBC project (or AoJ)](https://github.com/oracle/oracle-db-examples/tree/master/java/AoJ) 
initially created by Oracle.

The goal of this project is to improve on this example, and to provide an
ADBA (Asynchronous DataBase Access) driver that can be used with existing JDBC 
drivers.
