---
layout: post
title:  "End of asyncjdbc project"
date:   2019-09-28 09:00:00 +0200
categories: news
---
As recently [announced](https://mail.openjdk.java.net/pipermail/jdbc-spec-discuss/2019-September/000529.html) by Douglas Surber on the jdbc-spec-discuss mailinglist, Oracle is stopping development of ADBA.

Given this development, and the fact that the asyncjdbc project never really got off the ground, I'm announcing the formal end of the project.

The current state of the project will remain available on <https://gitlab.com/asyncjdbc/asyncjdbc>. The website <https://www.asyncjdbc.org/> will be closed on the next renewal of the domain (around April/May 2020), but <https://asyncjdbc.gitlab.io/> will remain available.
