---
layout: page
title: About asyncjdbc
permalink: /about/
---

## Project closed

As recently [announced](https://mail.openjdk.java.net/pipermail/jdbc-spec-discuss/2019-September/000529.html) by Douglas Surber on the jdbc-spec-discuss mailinglist, Oracle is stopping development of ADBA.

Given this development, and the fact that the asyncjdbc project never really got off the ground, I'm announcing the formal end of the project.

The current state of the project will remain available on <https://gitlab.com/asyncjdbc/asyncjdbc>. The website <https://www.asyncjdbc.org/> will be closed on the next renewal of the domain (around April/May 2020), but <https://asyncjdbc.gitlab.io/> will remain available.

## Description

The asyncjdbc project is an implementation of ADBA (Asynchronous DataBase Access) 
to wrap existing JDBC (Java DataBase Connectivity) drivers.

The project is based on the _AoJ:ADBA-over-JDBC_ example by Oracle in their
[oracle-db-example](https://github.com/oracle/oracle-db-examples) project. Oracle
released this as a partial implementation of ADBA with the expectation that the
community would build on this (see [this message](http://mail.openjdk.java.net/pipermail/jdbc-spec-discuss/2018-April/000264.html) 
from April 9th, 2018 with subject _AoJ : ADBA Over JDBC_ on the jdbc-spec-discuss mailinglist).

## Code

You can find the code on GitLab: <https://gitlab.com/asyncjdbc/asyncjdbc>

Building the project requires Java 10 or higher. Use `gradlew build` from the root to build.

## Mailing list

To subscribe to the asyncjdbc-dev mailing list, send a message 
to [asyncjdbc-dev+subscribe@googlegroups.com](mailto:asyncjdbc-dev+subscribe@googlegroups.com) 
or go directly to the [asyncjdbc-dev group](https://groups.google.com/forum/#!forum/asyncjdbc-dev)
and subscribe there.
