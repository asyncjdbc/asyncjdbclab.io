---
layout: page
title: Documentation
permalink: /docs/
---

## Project closed

As recently [announced](https://mail.openjdk.java.net/pipermail/jdbc-spec-discuss/2019-September/000529.html) by Douglas Surber on the jdbc-spec-discuss mailinglist, Oracle is stopping development of ADBA.

Given this development, and the fact that the asyncjdbc project never really got off the ground, I'm announcing the formal end of the project.

The current state of the project will remain available on <https://gitlab.com/asyncjdbc/asyncjdbc>. The website <https://www.asyncjdbc.org/> will be closed on the next renewal of the domain (around April/May 2020), but <https://asyncjdbc.gitlab.io/> will remain available.

## Documentation

There is no in-depth documentation yet. For a basic introduction see <https://gitlab.com/asyncjdbc/asyncjdbc>

## Javadoc

For asyncjdbc/adbaoverjdbc: <https://asyncjdbc.gitlab.io/asyncjdbc/adbaoverjdbc/docs/javadoc/>

For the time being (while ADBA is not part of Java yet), we also publish the ADBA javadoc: <https://asyncjdbc.gitlab.io/asyncjdbc/adba/docs/javadoc/>
